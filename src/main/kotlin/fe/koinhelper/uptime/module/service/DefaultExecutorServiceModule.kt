package fe.koinhelper.uptime.module.service

import org.koin.dsl.module
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService

val defaultScheduledExecutorModule = module {
    single<ScheduledExecutorService> {
        Executors.newScheduledThreadPool(1)
    }
}

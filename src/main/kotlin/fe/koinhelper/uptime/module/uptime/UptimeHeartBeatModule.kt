package fe.koinhelper.uptime.module.uptime

import fe.httpkt.Request
import fe.koinhelper.uptime.config.UptimeTrackerConfig
import org.koin.dsl.module

val uptimeHeartbeatModule = module {
    single {
        UptimeHeartbeat(get(), get())
    }
}

class UptimeHeartbeat(
    private val config: UptimeTrackerConfig,
    private val request: Request,
) {
    val ping = Runnable {
        request.get(config.pingUrl, forceSend = true)
    }

    val intervalMinutes = config.intervalMinutes
    val enabled = config.enabled
}

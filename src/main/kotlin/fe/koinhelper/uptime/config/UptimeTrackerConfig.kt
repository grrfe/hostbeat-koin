package fe.koinhelper.uptime.config

data class UptimeTrackerConfig(
    val enabled: Boolean,
    val pingUrl: String,
    val intervalMinutes: Long = 10,
)

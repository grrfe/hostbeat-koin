package fe.koinhelper.uptime.init

import fe.koinhelper.ext.KoinInit
import fe.koinhelper.uptime.module.uptime.UptimeHeartbeat
import org.koin.core.component.inject
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

object UptimeHeartBeatInit : KoinInit {
    private val module by inject<UptimeHeartbeat>()
    private val executor by inject<ScheduledExecutorService>()

    override fun init() {
        if (module.enabled) {
            executor.scheduleAtFixedRate(module.ping, 0, module.intervalMinutes, TimeUnit.MINUTES)
        }
    }
}

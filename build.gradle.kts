plugins {
    kotlin("jvm") version "1.7.10"
    java
    maven
    id("net.nemerosa.versioning") version "3.0.0"
}

group = "fe.koin.helper"
version = versioning.info.tag ?: versioning.info.full ?: "0.0.0"

repositories {
    mavenCentral()
    maven(url="https://jitpack.io")
}

dependencies {
    api("io.insert-koin:koin-core:3.2.1")
    api("com.gitlab.grrfe:koin-helper:2.0.3")
    api("com.gitlab.grrfe.httpkt:core:13.0.0-alpha.24")
}
